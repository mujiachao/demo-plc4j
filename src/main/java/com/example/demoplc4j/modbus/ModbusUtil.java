package com.example.demoplc4j.modbus;

import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.msg.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @program:  wingtech-boot-parent
 * @description: modbus读写工具类
 * @author:  Ricardo.Liu
 * @create:  2020-05-08
 * @since:  JDK 1.8
 **/
@Component
public class ModbusUtil {

    //从机默认值
    private final Integer slaveId = 1;

    @Autowired
    private ModbusConfig modbusConfig;
    private ModbusMaster master;

    public void connect(String ip, Integer port) {
        master = modbusConfig.getMaster(ip, port);
    }

    public void close(String ip) {
        modbusConfig.close(ip);
    }

    public boolean[] readCoilStatus(int offset, int numberOfRegister) throws ModbusTransportException {

        ReadCoilsRequest request = new ReadCoilsRequest(slaveId, offset, numberOfRegister);
        ReadCoilsResponse response = (ReadCoilsResponse) master.send(request);
        boolean[] booleans = response.getBooleanData();

        return valueRegroup(numberOfRegister, booleans);
    }

    /**
     * @Description:  读取外围设备输入的开关量，相当于功能码：02H-读离散输入状态
     * @Param:  [ip, offset, numberOfRegister]
     * @return:  boolean[]
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    public boolean[] readInputStatus(int offset, int numberOfRegister) throws ModbusTransportException {


        ReadDiscreteInputsRequest request = new ReadDiscreteInputsRequest(slaveId,offset, numberOfRegister);
        ReadDiscreteInputsResponse response = (ReadDiscreteInputsResponse) master.send(request);
        boolean[] booleans = response.getBooleanData();

        return valueRegroup(numberOfRegister, booleans);
    }

    /**
     * @Description:  读取保持寄存器数据，相当于功能码：03H-读保持寄存器
     * @Param:  [ip, offset, numberOfRegister]
     * @return:  short[]
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    public short[] readHoldingRegister(int offset, int numberOfRegister) throws ModbusTransportException {


        ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest(slaveId, offset, numberOfRegister);
        ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) master.send(request);
        return response.getShortData();
    }

    /**
     * @Description:  读取外围设备输入的数据，相当于功能码：04H-读输入寄存器
     * @Param:  [ip, offset, numberOfRegister]
     * @return:  short[]
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    public short[] readInputRegisters(int offset, int numberOfRegister) throws ModbusTransportException {


        ReadInputRegistersRequest request = new ReadInputRegistersRequest(slaveId, offset, numberOfRegister);
        ReadInputRegistersResponse response = (ReadInputRegistersResponse) master.send(request);
        return response.getShortData();
    }

    /**
     * @Description:  写单个（线圈）开关量数据，相当于功能码：05H-写单个线圈
     * @Param:  [ip, writeOffset, writeValue]
     * @return:  boolean
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    public boolean writeCoil(int writeOffset, boolean writeValue) throws ModbusTransportException {


        WriteCoilRequest request = new WriteCoilRequest(slaveId, writeOffset, writeValue);
        WriteCoilResponse response = (WriteCoilResponse) master.send(request);
        return !response.isException();
    }

    /**
     * @Description:  写多个开关量数据（线圈），相当于功能码：0FH-写多个线圈
     * @Param:  [ip, startOffset, data]
     * @return:  boolean
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    public boolean writeCoils(int startOffset, boolean[] data) throws ModbusTransportException {


        WriteCoilsRequest request = new WriteCoilsRequest(slaveId, startOffset, data);
        WriteCoilsResponse response = (WriteCoilsResponse) master.send(request);
        return !response.isException();

    }

    /**
     * @Description:  写单个保持寄存器，相当于功能码：06H-写单个保持寄存器
     * @Param:  [ip, writeOffset, writeValue]
     * @return:  boolean
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    public boolean writeHoldingRegister(int writeOffset, short writeValue) throws ModbusTransportException, ModbusInitException {


        WriteRegisterRequest request = new WriteRegisterRequest(slaveId, writeOffset, writeValue);
        WriteRegisterResponse response = (WriteRegisterResponse) master.send(request);
        return !response.isException();

    }

    /**
     * @Description:  写多个保持寄存器，相当于功能码：10H-写多个保持寄存器
     * @Param:  [ip, startOffset, data]
     * @return:  boolean
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    public boolean writeHoldingRegisters(int startOffset, short[] data) throws ModbusTransportException, ModbusInitException {


        WriteRegistersRequest request = new WriteRegistersRequest(slaveId, startOffset, data);
        WriteRegistersResponse response = (WriteRegistersResponse) master.send(request);
        return !response.isException();
    }

    /**
     * @Description:  转换工具，将Boolean转换成0，1
     * @Param:  [numberOfBits, values]
     * @return:  boolean[]
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    private  boolean[] valueRegroup(int numberOfBits, boolean[] values) {
        boolean[] bs = new boolean[numberOfBits];
        int temp = 1;
        for (boolean b : values) {
            bs[temp - 1] = b;
            temp++;
            if (temp > numberOfBits) {
                break;
            }
        }
        return bs;
    }
}
