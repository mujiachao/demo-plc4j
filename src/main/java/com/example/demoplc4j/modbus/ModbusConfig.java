package com.example.demoplc4j.modbus;

/**
 * @program:  wingtech-boot-parent
 * @description: modbus读写工具类
 * @author:  Ricardo.Liu
 * @create:  2020-05-08
 * @since:  JDK 1.8
 **/

import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.ip.IpParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.HashMap;

@Configuration
/*
 * 使用@Bean,就不用使用@Import来导入相应的类了，@Bean生成的bean的名字默认为方法名，由于hashMap使用很广泛，
 * 所以使用@Bean的方式引入依赖，这样在注入的时候可以指定名称，以免注入错误的对象
 * @Import({java.util.HashMap.class,com.serotonin.modbus4j.ModbusFactory.class})
 */
@Import(com.serotonin.modbus4j.ModbusFactory.class)
public class ModbusConfig {
    @Bean
    public HashMap<String, ModbusMaster> modbusMasterHashMap() {

        return new HashMap<>();
    }

    @Autowired
    private ModbusFactory modbusFactory;

    @Autowired
    @Qualifier("modbusMasterHashMap")
    private HashMap<String,ModbusMaster> masterMap;


    public ModbusMaster getMaster(String ip, Integer port) {

        ModbusMaster modbusMaster = masterMap.get(ip);
        if(modbusMaster == null) {
            setMaster(ip, port);
            modbusMaster = masterMap.get(ip);
        }
        return modbusMaster;
    }

    /**
     * @Description:  设置ip对应的modbus连接器
     * @Param:  [ip, port]
     * @return:  void
     * @throws:
     * @Author:  Ricardo.Liu
     * @Date:  2020/5/8
     */
    private void setMaster(String ip, Integer port) {

        ModbusMaster master;
        IpParameters params = new IpParameters();
        params.setHost(ip);
        params.setPort(port);
        //设置为true，会导致TimeoutException: request=com.serotonin.modbus4j.ip.encap.EncapMessageRequest@774dfba5",
        //params.setEncapsulated(true);
        master = modbusFactory.createTcpMaster(params, false);// TCP 协议
        try {
            //设置超时时间
            master.setTimeout(3*1000);
            //设置重连次数
            master.setRetries(3);
            //初始化
            master.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
        }
        masterMap.put(ip, master);
    }

    public void close(String ip) {
        ModbusMaster modbusMaster = masterMap.get(ip);
        if(modbusMaster != null) {
            masterMap.remove(ip);
            modbusMaster.destroy();
        }
    }
}
