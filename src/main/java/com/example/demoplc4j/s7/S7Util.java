package com.example.demoplc4j.s7;

import com.github.s7connector.api.DaveArea;
import com.github.s7connector.api.S7Connector;
import com.github.s7connector.api.factory.S7ConnectorFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;


/**
 * @author zyw
 * @version 1.0
 * @date 2021-10-19 15:57
 */

/**
 * S7协议连接PLC工具类
 * JAVA-PLC-RETURN
 * INT = DINT = INTEGER
 * STRING = STRING = STRING
 * Float = REAL = REAL
 * INT = DWORD = LONG
 * SHORT = WORD = INTEGER
 */
@Component
public class S7Util {

    private S7Connector s7Connector;

    /**
     * 区块 , byte数据,偏移量
     *
     * @param block
     * @param buffer
     * @param offset
     */
    public void writePlcRegister(Integer block, byte[] buffer, Integer offset) {
        s7Connector.write(DaveArea.DB, block, offset, buffer);
    }

    /**
     * 区块 , 多少位字节 , 偏移量
     *
     * @param block
     * @param buffer
     * @param offset
     * @return
     */
    public byte[] readPlcRegister(Integer block, Integer buffer, Integer offset) {
        return s7Connector.read(
                //选择区块
                DaveArea.DB,
                // 区块编号
                block,
                //字节
                buffer,
                //偏移
                offset);
    }


    /**
     * 通过IP和端口跟S7进行连接
     *
     * @param ip
     * @return
     */
    public void s7Connector(String ip, Integer port) {
        s7Connector = S7ConnectorFactory
                .buildTCPConnector()
                .withHost(ip)
                .withPort(port)
                .withTimeout(10000)
                .withRack(0)
                //optional
                .withSlot(1)
                //optional
                .build();
    }

    public void close() throws IOException {
        if (s7Connector != null) {
            s7Connector.close();
        }
    }

    /**
     * 读取PLC中的整型数据
     **/
    public void testReadPlcIntData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：数据长度
        //第四个参数：偏移量
        byte[] getBytes = s7Connector.read(DaveArea.DB, 6, 4, 0);

        Integer intData = new BigInteger(getBytes).intValue();
        System.out.println("getIntData:" + intData);

    }

    /**
     * 向PLC中写短整型(2字节，对应PLC INT类型)数据
     **/
    public void testWritePlcShortIntData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：偏移量
        //第四个参数：写入的数据 二进制数组byte[]
        short data = 15;
        byte[] bytes = ByteBuffer.allocate(2).putShort(data).array();
        System.out.println("bytes length:" + bytes.length);
        s7Connector.write(DaveArea.DB, 150, 0, bytes);

    }

    /**
     * 向PLC中写整型(4字节，对应PLC DINT类型)数据
     **/
    public void testWritePlcIntData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：偏移量
        //第四个参数：写入的数据 二进制数组byte[]
        int data = 15;
        byte[] bytes = ByteBuffer.allocate(4).putInt(data).array();
        System.out.println("bytes length:" + bytes.length);
        s7Connector.write(DaveArea.DB, 6, 0, bytes);

    }


    /**
     * 向PLC中读字符串
     **/
    public void testReadPlcStrData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：数据长度
        //第四个参数：偏移量
        byte[] getBytes = s7Connector.read(DaveArea.DB, 150, 15, 2);

        String string = new String(getBytes);
        System.out.println("getStringData:" + string.trim());

    }

    /**
     * 向PLC中写字符串
     **/
    public void testWritePlcStrData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：偏移量
        //第四个参数：写入的数据 二进制数组byte[]
        String str = "hello world";//此处要补齐后面的空格，比如字符串是50的长度区域，那就得想办法将最终bytes控制在50
        int strLen = 15;//字符串字段的长度
        if (str.length() < strLen) {//用空格填充
            str = StringUtils.rightPad(str, strLen, " ");
        } else {//最多控制在区域大小以内
            str = str.substring(0, strLen);
        }
        s7Connector.write(DaveArea.DB, 150, 2, str.getBytes(StandardCharsets.UTF_8));
        System.out.println("write length:" + str.getBytes(StandardCharsets.UTF_8).length);

    }

    /**
     * 读取PLC中的浮点型float(real 4字节)数据
     * java float : plc Real 4 字节
     * java double : plc LReal 8 字节
     **/
    public void testReadPlcRealData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：数据长度
        //第四个参数：偏移量
        //byte [] bytes = ByteBuffer.allocate(8).putDouble(1729.1729).array();
        //byte [] bytes = { 64, -101, 4, -79, 12, -78, -107, -22 };
        //System.out.println();

        byte[] getBytes = s7Connector.read(DaveArea.DB, 150, 4, 64);
        Float getValue = ByteBuffer.wrap(getBytes).getFloat();
        System.out.println("getFloatData:" + getValue);

    }

    /**
     * 读取PLC中的浮点型Double(8字节)数据
     * java float : plc Real 4 字节
     * java double : plc LReal 8 字节
     **/
    public void testReadPlcLRealData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：数据长度
        //第四个参数：偏移量
        //byte [] bytes = ByteBuffer.allocate(8).putDouble(1729.1729).array();
        //byte [] bytes = { 64, -101, 4, -79, 12, -78, -107, -22 };
        //System.out.println();

        byte[] getBytes = s7Connector.read(DaveArea.DB, 6, 8, 4);
        Double getValue = ByteBuffer.wrap(getBytes).getDouble();
        System.out.println("getDoubleData:" + getValue);

    }

    /**
     * 写入PLC中的浮点型数据
     * java float : plc Real 4 字节
     * java double : plc LReal 8 字节
     **/
    public void testWritePlcRealData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：偏移量
        //第四个参数：写入的数据 二进制数组byte[]
        float data = new Float(5.54);
        byte[] bytes = ByteBuffer.allocate(4).putFloat(data).array();
        System.out.println("bytes length:" + bytes.length);
        s7Connector.write(DaveArea.DB, 150, 64, bytes);

    }

    /**
     * 写入PLC中的浮点型LREAL(java中是double)数据
     * java float : plc Real 4 字节
     * java double : plc LReal 8 字节
     **/
    public void testWritePlcLRealData() {

        //第一个参数：DaveArea.DB 表示读取PLC的地址区域为DB
        //第二个参数：DB块地址，若plc中是DB1000，则填1000
        //第三个参数：偏移量
        //第四个参数：写入的数据 二进制数组byte[]
        float data = new Float(15.5);
        byte[] bytes = ByteBuffer.allocate(8).putDouble(data).array();
        System.out.println("bytes length:" + bytes.length);
        s7Connector.write(DaveArea.DB, 6, 4, bytes);

    }

}
