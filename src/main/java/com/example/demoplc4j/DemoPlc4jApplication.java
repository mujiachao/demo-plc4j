package com.example.demoplc4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class DemoPlc4jApplication {

    public static void main(String[] args) {
//      //防止依赖循环
        try {
            SpringApplication springApplication = new SpringApplication(DemoPlc4jApplication.class);
            springApplication.setAllowCircularReferences(Boolean.TRUE);
            springApplication.run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
