package com.example.demoplc4j;

import com.example.demoplc4j.s7.S7Util;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
@Slf4j
class S7Test1 {

    @Autowired
    S7Util s7Util;
    @Test
    void contextLoads() throws Exception {
        String ip = "127.0.0.1";
        Integer port = 102;
        s7Util.s7Connector(ip, port);
        s7Util.testWritePlcIntData();
        s7Util.testReadPlcIntData();

        s7Util.testWritePlcStrData();
        s7Util.testReadPlcStrData();

        s7Util.close();

    }



}
