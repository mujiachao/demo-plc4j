package com.example.demoplc4j;

import com.example.demoplc4j.modbus.ModbusUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.plc4x.java.PlcDriverManager;
import org.apache.plc4x.java.api.PlcConnection;
import org.apache.plc4x.java.api.messages.*;
import org.apache.plc4x.java.api.model.PlcSubscriptionHandle;
import org.apache.plc4x.java.api.types.PlcResponseCode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

@SpringBootTest
@Slf4j
class ModbusTest2 {

    @Autowired
    ModbusUtil modbusUtil;
    @Test
    void contextLoads() throws Exception {
        String ip = "127.0.0.1";
        Integer port = 502;
        modbusUtil.connect(ip, port);

        modbusUtil.writeCoils(0, new boolean[]{false, true, true, false});
        boolean[] booleans = modbusUtil.readCoilStatus(0, 4);
        modbusUtil.writeHoldingRegisters(100, new short[]{1, 2, 3, 5});
        short[] shorts = modbusUtil.readHoldingRegister(100, 4);
        System.out.println(Arrays.toString(shorts));
        System.out.println(Arrays.toString(booleans));

        modbusUtil.close(ip);
    }


}
