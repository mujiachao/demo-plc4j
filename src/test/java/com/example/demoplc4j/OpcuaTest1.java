package com.example.demoplc4j;

import lombok.extern.slf4j.Slf4j;
import org.apache.plc4x.java.PlcDriverManager;
import org.apache.plc4x.java.api.PlcConnection;
import org.apache.plc4x.java.api.messages.*;
import org.apache.plc4x.java.api.model.PlcSubscriptionHandle;
import org.apache.plc4x.java.api.types.PlcResponseCode;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

@SpringBootTest
@Slf4j
class OpcuaTest1 {


    @Test
    void contextLoads() throws Exception {
        String connectionString = "opcua:tcp://10.182.95.237:49320";
        PlcConnection plcConnection = null;
        try {
            plcConnection = new PlcDriverManager().getConnection(connectionString);
            if (!plcConnection.isConnected()) {
                log.info("未连接");
            } else {
                log.info("连接成功");
            }
            read(plcConnection);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (plcConnection != null) {
                plcConnection.close();
            }
        }
    }

    private void read(PlcConnection plcConnection) throws ExecutionException, InterruptedException {
        if (!plcConnection.getMetadata().canRead()) {
            log.error("This connection doesn't support reading.");
            return;
        }
        // Create a new read request:
        // - Give the single item requested the alias name "value"
        PlcReadRequest.Builder builder = plcConnection.readRequestBuilder();
        builder.addItem("value-1", "ns=2;s=通道 2.设备 1.组1.test");
        PlcReadRequest readRequest = builder.build();
        PlcReadResponse response = readRequest.execute().get();
        for (String fieldName : response.getFieldNames()) {
            if (response.getResponseCode(fieldName) == PlcResponseCode.OK) {
                int numValues = response.getNumberOfValues(fieldName);
                // If it's just one element, output just one single line.
                if (numValues == 1) {
                    log.info("Value[" + fieldName + "]: " + response.getObject(fieldName));
                }
                // If it's more than one element, output each in a single row.
                else {
                    log.info("Value[" + fieldName + "]:");
                    for (int i = 0; i < numValues; i++) {
                        log.info(" - " + response.getObject(fieldName, i));
                    }
                }
            }
            // Something went wrong, to output an error message instead.
            else {
                log.error("Error[" + fieldName + "]: " + response.getResponseCode(fieldName).name());
            }
        }
    }
    private void write(PlcConnection plcConnection) throws ExecutionException, InterruptedException {
        // Check if this connection support reading of data.
        if (!plcConnection.getMetadata().canWrite()) {
            log.error("This connection doesn't support writing.");
            return;
        }
        // Create a new read request:
        // - Give the single item requested the alias name "value"
        PlcWriteRequest.Builder builder = plcConnection.writeRequestBuilder();
        builder.addItem("value-1", "ns=2;s=通道 2.设备 1.组1.test;USINT", false);
        PlcWriteRequest writeRequest = builder.build();
        PlcWriteResponse response = writeRequest.execute().get();
        for (String fieldName : response.getFieldNames()) {
            if(response.getResponseCode(fieldName) == PlcResponseCode.OK) {
                log.info("Value[" + fieldName + "]: successfully written to device.");
            }
            // Something went wrong, to output an error message instead.
            else {
                log.error("Error[" + fieldName + "]: " + response.getResponseCode(fieldName).name());
            }
        }
    }
    private void subscribe(PlcConnection plcConnection) throws ExecutionException, InterruptedException {
        // Check if this connection support subscribing to data.
        if (!plcConnection.getMetadata().canSubscribe()) {
            log.error("This connection doesn't support subscribing.");
            return;
        }
        // Create a new subscription request:
// - Give the single item requested an alias name
        PlcSubscriptionRequest.Builder builder = plcConnection.subscriptionRequestBuilder();
        builder.addChangeOfStateField("value-1", "{some address}");
        builder.addCyclicField("value-2", "{some address}", Duration.ofMillis(1000));
        builder.addEventField("value-3", "{some alarm address}");
        PlcSubscriptionRequest subscriptionRequest = builder.build();
        PlcSubscriptionResponse response = subscriptionRequest.execute().get();
        for (String subscriptionName : response.getFieldNames()) {
            final PlcSubscriptionHandle subscriptionHandle = response.getSubscriptionHandle(subscriptionName);
            subscriptionHandle.register(plcSubscriptionEvent -> {
                for (String fieldName : plcSubscriptionEvent.getFieldNames()) {
                    System.out.println(plcSubscriptionEvent.getPlcValue(fieldName));
                }
            });
        }
    }

}
