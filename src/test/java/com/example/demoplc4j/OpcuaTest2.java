package com.example.demoplc4j;

import com.example.demoplc4j.opcua.OpcUaClientTest;
import lombok.extern.slf4j.Slf4j;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
@Slf4j
class OpcuaTest2 {


    @Test
    void contextLoads() throws Exception {
        //创建OPC UA客户端
        OpcUaClient opcUaClient = OpcUaClientTest.createClient();

        //开启连接
        opcUaClient.connect().get();

        //遍历节点
//        browseNode(opcUaClient, null);
        //写
        OpcUaClientTest.writeNodeValue(opcUaClient);

        //读
        OpcUaClientTest.readNode(opcUaClient);



        //订阅
        OpcUaClientTest.subscribe(opcUaClient);

        //批量订阅
        //managedSubscriptionEvent(opcUaClient);

        //关闭连接
        opcUaClient.disconnect().get();
    }



}
